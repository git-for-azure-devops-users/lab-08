# Git for TFS users
Lab 08: Reverting and cherry-picking changes

---

# Tasks

 - Revert a bug in a feature branch
 
 - Cherry-pick a change from another branch
 
 - Tag the fixed commit 

---

## Preparations

 - Clone the lab repository from visual studio
```
http://<server-ip>:8080/tfs/DefaultCollection/git-workshop/_git/demo-app-lab-08
```

- Open the index.html located in the root directory to see the application

&nbsp;
<img alt="Image" src="https://gitlab.com/sela-git-advanced-workshop/Lab-03/raw/master/Images/3.1.png" border="1">
&nbsp;

- Create a feature branch called "feature/<your-user>"

---

## Revert a bug

 - Inspect the master branch history, a bug was introduced in the commit "5f372c5"

 - Let's fix the bug by revert the commit "5f372c5", select the commit in the history view and click "revert"

&nbsp;
<img alt="Image" src="https://docs.microsoft.com/en-us/azure/devops/repos/git/_img/vs_revert_changes.png?view=azure-devops" border="1">
&nbsp;

 - Reolve the conflict and open the index.html to see the application

---

## Cherry-pick a change from another branch

 - Inspect the feature branch history, a new page title was introduced in the commit "58c98948"

 - Let's cherry-pick this commit to apply it in the master branch, in the history view select the commit and click "cherry-pick"

&nbsp;
<img alt="Image" src="https://docs.microsoft.com/en-us/azure/devops/repos/git/_img/vscherrypick.gif?view=azure-devops" border="1">
&nbsp;

 - Resolve the conflict and commit the changes, then open the application to ensure that was updated
 

## Tag the fixed commit

 - Add a "bugfix-<your-user>" tag in the local repository to the current commit from the Tags view in Team Explorer

&nbsp;
<img alt="Image" src="https://docs.microsoft.com/en-us/azure/devops/repos/git/_img/git-tags/create-tag-current-branch-vs.png?view=azure-devops" border="1">
&nbsp;

 - Push your feature branch and check it in the web portal under the "Commits" tab (note that tags are not pushed by default)

&nbsp;
<img alt="Image" src="https://docs.microsoft.com/en-us/azure/devops/repos/git/_img/git-tags/create-tag-current-branch-vs.png?view=azure-devops" border="1">
&nbsp;

- Push the created tag to the remote repository

&nbsp;
<img alt="Image" src="https://docs.microsoft.com/en-us/azure/devops/repos/git/_img/git-tags/create-tag-current-branch-vs.png?view=azure-devops" border="1">
&nbsp;

 - Refresh the web portal to see the pushed tag
